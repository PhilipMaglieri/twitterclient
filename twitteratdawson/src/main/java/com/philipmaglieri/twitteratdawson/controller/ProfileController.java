/**
 * This Controller will handle the profile fxml
 * that contains the follow button and message button
 */

package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.business.TwitterEngine;
import com.philipmaglieri.twitteratdawson.business.TwitterInfo;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoInterface;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import twitter4j.Relationship;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

public class ProfileController {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ProfileController.class); 
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private final Twitter twitter = twitterEngine.getTwitterinstance();
    private TwitterInfoInterface info;
    private Stage stage;
    private ProfileController profileController;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="profileBorderPane"
    private BorderPane profileBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="profileHBox"
    private HBox profileHBox; // Value injected by FXMLLoader

    @FXML // fx:id="profileImageView"
    private ImageView profileImageView; // Value injected by FXMLLoader

    @FXML // fx:id="followButton"
    private Button followButton; // Value injected by FXMLLoader
    
    @FXML // fx:id="profileMessageButton"
    private Button profileMessageButton; // Value injected by FXMLLoader

    @FXML // fx:id="profileTextArea"
    private TextArea profileTextArea; // Value injected by FXMLLoader

    private BorderPane sendDirectMessageBorderPane;
    private SendDirectMessageController sendDirectMessageController;

    /**
     * handler for clicking on the follow button 
     * @param event 
     */
    @FXML
    void onFollowButtonClick(ActionEvent event) {
        LOG.info("follow clicked");
        try {
            User user = twitter.showUser(twitter.getId());
            Relationship friend = twitter.showFriendship(user.getScreenName(), info.getHandle());
            if(friend.isSourceFollowingTarget()){
            twitterEngine.follow(info.getHandle());
            }
            else{
            twitter.destroyFriendship(info.getHandle());
            }
        } catch (TwitterException ex) {
            LOG.error("could not follow",ex);
        }
    }
    
    /**
     * handler for messaging the person from their profile
     * @param event 
     */
    @FXML
    void onProfileMessageButtonClick(ActionEvent event) {
        LOG.info("profile message cliked");
        try {
            twitterEngine.sendDirectMessage(info.getHandle(), profileTextArea.getText());
            profileTextArea.setText("");
        } catch (TwitterException ex) {
            LOG.error("unable to send DM", ex);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert profileBorderPane != null : "fx:id=\"profileBorderPane\" was not injected: check your FXML file 'profile.fxml'.";
        assert profileHBox != null : "fx:id=\"profileHBox\" was not injected: check your FXML file 'profile.fxml'.";
        assert profileImageView != null : "fx:id=\"profileImageView\" was not injected: check your FXML file 'profile.fxml'.";
        assert followButton != null : "fx:id=\"followButton\" was not injected: check your FXML file 'profile.fxml'.";
        assert profileMessageButton != null : "fx:id=\"profileMessageButton\" was not injected: check your FXML file 'profile.fxml'.";
        assert profileTextArea != null : "fx:id=\"profileTextArea\" was not injected: check your FXML file 'profile.fxml'.";
        
        //profileBorderPane.setCenter(sendDirectMessageBorderPane);
        
    }
    
    /**
     * sets the user and image in the image view
     * @param info 
     */
    public void setUser(TwitterInfoInterface info){
        this.info = info;
        setImageView();
    }
    
    private void setImageView(){
        Image image = new Image(this.info.getImageURL());
        this.profileImageView.imageProperty().set(image);
    }
    
    /**
     * loads the stage for the profile
     * @return 
     */
    public Stage loadStage() {
        try {
            this.stage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainUIController.class.getResource("/fxml/profile.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
            profileBorderPane = (BorderPane) loader.load();
            profileController = loader.getController();
            Scene scene = new Scene(profileBorderPane);
            scene.getStylesheets().add("/styles/mainUI.css");
            stage.setScene(scene);
            
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
        return stage;
    }
    
    /**
     * loads the controller for the profile
     * @return 
     */
    public ProfileController loadProfileController(){
                try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainUIController.class.getResource("/fxml/profile.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
            profileBorderPane = (BorderPane) loader.load();
            profileController = loader.getController();           
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
        return profileController;
    }
}
