/**
 * controller fot the timeline fxml
 */
package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.business.TwitterEngine;
import com.philipmaglieri.twitteratdawson.business.TwitterInfo;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoCell;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoInterface;
import com.philipmaglieri.twitteratdawson.business.TwitterTimeLineTask;
import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Stage;
import org.apache.logging.log4j.core.util.Loader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeLineController {
    
    private final static Logger LOG = LoggerFactory.getLogger(TimeLineController.class);
    private TwitterTimeLineTask task;
    private final TwitterEngine twitterEngine = new TwitterEngine();
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    
    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    
    @FXML // fx:id="timelineBorderPane"
    private BorderPane timelineBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="listView"
    private ListView<TwitterInfoInterface> listView; // Value injected by FXMLLoader

    @FXML // fx:id="showTimelineButton"
    private Button showTimelineButton; // Value injected by FXMLLoader

    private BorderPane profileBorderPane;
    private ProfileController profileController;
    
    public TimeLineController() {
        super();
        
    }

    /**
     * Handler for displaying the timeline for the show button
     *
     * @param event
     */
    @FXML
    void onShowTimelineButtonClick(ActionEvent event) {
        LOG.info("Show timeline clicked");
        //listView.getItems().clear();
        if (task == null) {
            task = new TwitterTimeLineTask(listView.getItems());
            
            showTimelineButton.setText(resources.getString("next"));
        }
        try {
            task.fillTimeLine();
        } catch (Exception ex) {
            LOG.error("Unable to display timeline", ex);
        }
        
    }
    
    /**
     * sets up the borderpane after initializing 
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert timelineBorderPane != null : "fx:id=\"homeBorderPane\" was not injected: check your FXML file 'timeline.fxml'.";
        assert listView != null : "fx:id=\"listView\" was not injected: check your FXML file 'timeline.fxml'.";
        assert showTimelineButton != null : "fx:id=\"showTimelineButton\" was not injected: check your FXML file 'timeline.fxml'.";
        
        timelineBorderPane.setTop(getHBoxButtons());
        timelineBorderPane.setCenter(getHBoxView());
        createProfileBorderPane();
        
    }
    
    /**
     * creates hbox for  top of border pane
     * @return 
     */
    private Node getHBoxButtons() {
        HBox node = new HBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);
        node.getChildren().add(showTimelineButton);
        return node;
    }
    
    /**
     * creates the view for a tweet
     * @return 
     */
    private Node getHBoxView() {
        HBox node = new HBox();
        node.setPadding(new Insets(0, 10, 10, 10));
        node.setSpacing(10);
        ObservableList<TwitterInfoInterface> list = FXCollections.observableArrayList();
        
        listView.setItems(list);
        listView.setPrefWidth(900);
        listView.setCellFactory(p -> new TwitterInfoCell());
        
        node.getChildren().addAll(listView);

        // Event handler when a cell is selected
       /** listView.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends TwitterInfo> ov, TwitterInfo t, TwitterInfo t1) -> {
                    if (t != null) {
                        LOG.debug("Previous handle: " + t.getHandle());
                    }
                    if (t1 != null) {
                        LOG.debug("New handle: " + t1.getHandle());
                    }
                }); **/
        return node;
    }
    
    /**
     * creates the profile border pane so that it can be displayed later
     */
    public void createProfileBorderPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/profile.fxml"));
            profileBorderPane = (BorderPane) loader.load();
            profileController = loader.getController();
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
    }
    
    /**
     * handles the reply click to send a message to the user that sent the tweet
     * @param event 
     */
    public void handleReplyClick(ActionEvent event) {
        
    }
    
}
