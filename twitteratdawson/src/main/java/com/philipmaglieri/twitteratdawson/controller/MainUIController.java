/**
 *
 * This controller handles the mainUI.fxml and its components
 *
 * inspired by https://gitlab.com/omniprof/javafx_properties_demo/tree/master/src/main/java/com/kenfogel/javafx_properties_demo
 *
 */
package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.presentation.MainApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class MainUIController {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MainUIController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="masterBorderPane"
    private BorderPane masterBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="timelineButton"
    private Button timelineButton; // Value injected by FXMLLoader

    @FXML // fx:id="tweetButton"
    private Button tweetButton; // Value injected by FXMLLoader

    @FXML // fx:id="directMessageButton"
    private Button directMessageButton; // Value injected by FXMLLoader
    
    @FXML // fx:id="receiveDirectMessageButton"
    private Button receiveDirectMessageButton; // Value injected by FXMLLoader
    
    @FXML // fx:id="myProfileButton"
    private Button myProfileButton; // Value injected by FXMLLoader

    @FXML // fx:id="helpButton"
    private Button helpButton; // Value injected by FXMLLoader

    @FXML // fx:id="exitButton"
    private Button exitButton; // Value injected by FXMLLoader

    private BorderPane sendDirectMessageBorderPane;
    private SendDirectMessageController sendDirectMessageController;
    private BorderPane timelineBorderPane;
    private TimeLineController timelineController;
    private BorderPane tweetBorderPane;
    private SendTweetController sendTweetController;
    private BorderPane profileBorderPane;
    private ProfileController profileController;
    private BorderPane receiveDirectMessagesPane;
    private ReceiveDirectMessageController receiveDirectMessageController;
    private BorderPane myProfileBorderPane;
    private MyProfileController myProfileController;
    private BorderPane helpBorderPane;
    private HelpController helpController;

    /**
     * Handler for changing the visible pane to the direct message pane
     *
     * @param event
     */
    @FXML
    void onDirectMessageClick(ActionEvent event) {
        LOG.info("Direct Message button clicked");
        masterBorderPane.setCenter(sendDirectMessageBorderPane);
    }

    /**
     * exits program
     *
     * @param event
     */
    @FXML
    void onExitClick(ActionEvent event) {
        LOG.info("Exit button clicked");
        Platform.exit();
    }

    /**
     * Handler to send to twitter help page
     *
     * @param event
     */
    @FXML
    void onHelpClick(ActionEvent event) {
        LOG.info("Help button clicked");
        masterBorderPane.setCenter(helpBorderPane);
    }
    
    /**
     * handler to send to my profile page
     * @param event 
     */
    @FXML
    void onMyProfileButtonClick(ActionEvent event) {
        try {
            myProfileController.setMyImage();
        } catch (TwitterException ex) {
            LOG.error("twitter error",ex);
        }
        masterBorderPane.setCenter(myProfileBorderPane);
    }
    
    /**
     * handler to change to receive DMs
     * @param event 
     */
    @FXML
    void onReceiveDirectMessageButtonClick(ActionEvent event) {
        LOG.info("receive DM button clicked");
        masterBorderPane.setCenter(receiveDirectMessagesPane);
    }

    /**
     * Handler for changing the visible pane to the home pane
     *
     * @param event
     */
    @FXML
    void onTimelineClick(ActionEvent event) {
        LOG.info("Home button clicked");
        masterBorderPane.setCenter(timelineBorderPane);
    }

    /**
     * Handler for changing the visible pane to the tweeting pane
     *
     * @param event
     */
    @FXML
    void onTweetClick(ActionEvent event) {
        LOG.info("Tweet button clicked");
        masterBorderPane.setCenter(tweetBorderPane);
    }

    /**
     * initializes the visible pane to home
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert masterBorderPane != null : "fx:id=\"masterBorderPane\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert timelineButton != null : "fx:id=\"timelineButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert tweetButton != null : "fx:id=\"tweetButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert directMessageButton != null : "fx:id=\"directMessageButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert receiveDirectMessageButton != null : "fx:id=\"receiveDirectMessageButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert myProfileButton != null : "fx:id=\"myProfileButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert helpButton != null : "fx:id=\"helpButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        assert exitButton != null : "fx:id=\"exitButton\" was not injected: check your FXML file 'mainUI.fxml'.";
        timelineButton.setTooltip(new Tooltip(resources.getString("TimelineToolTip")));
        tweetButton.setTooltip(new Tooltip(resources.getString("TweetToolTip")));
        directMessageButton.setTooltip(new Tooltip(resources.getString("DirectMessageToolTip")));
        receiveDirectMessageButton.setTooltip(new Tooltip(resources.getString("ReceiveDirectMessageToolTip")));
        myProfileButton.setTooltip(new Tooltip(resources.getString("MyProfileToolTip")));
        helpButton.setTooltip(new Tooltip(resources.getString("HelpToolTip")));
        exitButton.setTooltip(new Tooltip(resources.getString("ExitToolTip")));

        createSendDirectMessageBorderPane();
        createTimelineBorderPane();
        createTweetBorderPane();
        createReceiveDirectMessagesBorderPane();
        createMyProfileBorderPane();
        createHelpBorderPane();
        masterBorderPane.setCenter(timelineBorderPane);
    }

    /**
     * create DM pane
     */
    public void createSendDirectMessageBorderPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/senddirectmessage.fxml"));
            sendDirectMessageBorderPane = (BorderPane) loader.load();
            sendDirectMessageController = loader.getController();
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
    }

    /**
     * create timeline pane
     */
    private void createTimelineBorderPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/timeline.fxml"));
            timelineBorderPane = (BorderPane) loader.load();
            timelineController = loader.getController();
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
    }

    /**
     * create tweet pane
     */
    private void createTweetBorderPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/sendtweet.fxml"));
            tweetBorderPane = (BorderPane) loader.load();
            sendTweetController = loader.getController();
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
    }

    /**
     * creates the DM pane
     */
    private void createReceiveDirectMessagesBorderPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/receivedirectmessage.fxml"));
            receiveDirectMessagesPane = (BorderPane) loader.load();
            receiveDirectMessageController = loader.getController();
        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }
    }
    
    /**
     * creates the myprofile pane
     */
    private void createMyProfileBorderPane(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/myprofile.fxml"));
            myProfileBorderPane = (BorderPane) loader.load();
            myProfileController = loader.getController();
        } catch (IOException e){
            LOG.error(null,e);
        }
        
    }
    
    private void createHelpBorderPane(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainUIController.class.getResource("/fxml/help.fxml"));
            helpBorderPane = (BorderPane) loader.load();
            helpController = loader.getController();
        } catch (IOException e){
            LOG.error(null,e);
        }
    }
}
