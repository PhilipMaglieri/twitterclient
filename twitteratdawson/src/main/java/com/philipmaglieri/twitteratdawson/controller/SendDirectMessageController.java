/**
 * Controller to handle the sending direct messages
 */
package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.business.TwitterEngine;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

public class SendDirectMessageController {

    private final static Logger LOG = LoggerFactory.getLogger(SendTweetController.class);
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private final Twitter twitter = twitterEngine.getTwitterinstance();
    private List<DirectMessage> messages;
    List<String> friendsNames = new ArrayList<String>();
    HashMap<String, Long> friendIds = new HashMap<>();
    private User user;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="directMessageBorderPane"
    private BorderPane directMessageBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="directMessageHandleText"
    private TextField directMessageHandleText; // Value injected by FXMLLoader

    @FXML // fx:id="friendsList"
    private ListView<String> friendsList; // Value injected by FXMLLoader

    @FXML // fx:id="directMessageList"
    private ListView<String> directMessageList; // Value injected by FXMLLoader

    @FXML // fx:id="sendDirectmessageButton"
    private Button sendDirectmessageButton; // Value injected by FXMLLoader

    @FXML // fx:id="directMesssageTextArea"
    private TextArea directMesssageTextArea; // Value injected by FXMLLoader

    /**
     * handler for send dm button sends a direct message to the twitter handle
     * in the textfield
     *
     * @param event
     */
    @FXML
    void onSendDirectMessageButtonClick(ActionEvent event) {

        LOG.info("Send DMbutton clicked");
        if(!(directMessageHandleText.getText().equals("")))
        try {
            twitterEngine.sendDirectMessage(directMessageHandleText.getText(), directMesssageTextArea.getText());
            directMesssageTextArea.setText("");
            directMessageHandleText.setText("");
        } catch (TwitterException ex) {
            LOG.error("unable to send DM", ex);
        }   
        else{
            try{
                twitterEngine.sendDirectMessage(friendIds.get(friendsList.getSelectionModel().getSelectedItem()), directMesssageTextArea.getText());
                directMesssageTextArea.setText("");
            }catch(TwitterException ex){
                LOG.error("unable to send DM",ex);
            }
            
        }
        //getFriends();
        //setListenerFriendsList();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert directMessageBorderPane != null : "fx:id=\"directMessageBorderPane\" was not injected: check your FXML file 'senddirectmessage.fxml'.";
        assert directMessageHandleText != null : "fx:id=\"directMessageHandleText\" was not injected: check your FXML file 'senddirectmessage.fxml'.";
        assert friendsList != null : "fx:id=\"friendsList\" was not injected: check your FXML file 'senddirectmessage.fxml'.";
        assert directMessageList != null : "fx:id=\"directMessageList\" was not injected: check your FXML file 'senddirectmessage.fxml'.";
        assert directMesssageTextArea != null : "fx:id=\"directMesssageTextArea\" was not injected: check your FXML file 'senddirectmessage.fxml'.";
        assert sendDirectmessageButton != null : "fx:id=\"sendDirectmessageButton\" was not injected: check your FXML file 'senddirectmessage.fxml'.";

        getFriends();
        setListenerFriendsList();
        directMessageHandleText.setText("");
    }

    private void getFriends() {
        User friend = null;

        try {
            this.user = twitter.showUser(twitter.getId());
            messages = twitterEngine.getDirectMessage();
            for (DirectMessage message : messages) {
                friend = twitter.showUser(message.getSenderId());
                if (!(friendsNames.contains(friend.getScreenName()))) {
                    friendIds.put(friend.getScreenName(), friend.getId());
                    friendsNames.add(friend.getScreenName());
                    friendsList.getItems().add(friend.getScreenName());
                }
            }
        } catch (TwitterException ex) {
            LOG.error("could not retreive friend list", ex);
        }
    }

    private void setListenerFriendsList() {
        
        friendsList.getSelectionModel().selectedItemProperty()
                .addListener((ov, s, s1) -> {
                    directMessageList.getItems().clear();
                    for (var message : messages) {
                        //check if dm to self
                        if (message.getSenderId() == friendIds.get(friendsList.getSelectionModel().getSelectedItem())
                           && message.getRecipientId() == friendIds.get(friendsList.getSelectionModel().getSelectedItem())) {
                            directMessageList.getItems().add(message.getText());
                        }
                    
                        else if (message.getSenderId() == friendIds.get(friendsList.getSelectionModel().getSelectedItem())
                            || message.getRecipientId() == friendIds.get(friendsList.getSelectionModel().getSelectedItem())) {
                            
                            if(message.getSenderId() == (friendIds.get(friendsList.getSelectionModel().getSelectedItem()))){
                                directMessageList.getItems().add(friendsList.getSelectionModel().getSelectedItem()+" "+message.getText());
                            }
                            else{
                                directMessageList.getItems().add(this.user.getName()+" "+message.getText());
                            }
                        }
                    }
                });
    }
}
