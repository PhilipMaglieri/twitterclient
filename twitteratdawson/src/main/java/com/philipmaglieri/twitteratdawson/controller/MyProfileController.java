/**
 * Controller for the profile pane
 */

package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.business.TwitterEngine;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoCell;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoInterface;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoNoStatus;
import com.philipmaglieri.twitteratdawson.business.TwitterTimeLineTask;
import com.philipmaglieri.twitteratdawson.persistence.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

public class MyProfileController {
    
    private final static Logger LOG = LoggerFactory.getLogger(SendTweetController.class);
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private final Twitter twitter = twitterEngine.getTwitterinstance();
    private List<DirectMessage> tweets;
    private TwitterTimeLineTask task;
    private User user;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="myProfileBorderPane"
    private BorderPane myProfileBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="myProfileImageView"
    private ImageView myProfileImageView; // Value injected by FXMLLoader

    @FXML // fx:id="mentionsButton"
    private Button mentionsButton; // Value injected by FXMLLoader

    @FXML // fx:id="retweetsButton"
    private Button retweetsButton; // Value injected by FXMLLoader

    @FXML // fx:id="tweetsRetweetedButton"
    private Button tweetsRetweetedButton; // Value injected by FXMLLoader
    
    @FXML // fx:id="getSavedTweetsButton"
    private Button getSavedTweetsButton; // Value injected by FXMLLoader

    @FXML // fx:id="searchTextField"
    private TextField searchTextField; // Value injected by FXMLLoader

    @FXML // fx:id="searchButton"
    private Button searchButton; // Value injected by FXMLLoader

    @FXML // fx:id="myProfileListView"
    private ListView<TwitterInfoInterface> myProfileListView; // Value injected by FXMLLoader

    /**
     * Handler for the getsavedtweets button
     * request is made to the database using DAO object
     * @param event 
     */
    @FXML
    void onGetSavedTweetsButtonClick(ActionEvent event) {
        LOG.info("show saved tweets clicked ");
        TweetDAOImpl database = new TweetDAOImpl();
        myProfileListView.getItems().clear();
        if(task == null){
            task = new TwitterTimeLineTask(myProfileListView.getItems());
            //mentionsButton.setText(resources.getString("NextMentions"));
        }
        try{
            task.fillTimeLineFromDatabase();
        } catch (SQLException ex) {
            LOG.error("Unable to display mentions", ex);
        }
    }
    
    /**
     * handler for the mentionsButton click 
     * gets the tweets where the user is mentioned
     * @param event 
     */
    @FXML
    void onMentionsButtonClick(ActionEvent event) {
        LOG.info("mentions clicked");
        myProfileListView.getItems().clear();
        if(task == null){
            task = new TwitterTimeLineTask(myProfileListView.getItems());
            //mentionsButton.setText(resources.getString("NextMentions"));
        }
        try{
            task.fillMentionsTimeLine();
        } catch (Exception ex) {
            LOG.error("Unable to display mentions", ex);
        }
    }
    /**
     * handler for the retweetsbutton click
     * gets the user's retweets
     * @param event 
     */
    @FXML
    void onRetweetsButtonClick(ActionEvent event) {
        LOG.info("retweets clicked");
        myProfileListView.getItems().clear();
        if(task == null){
            task = new TwitterTimeLineTask(myProfileListView.getItems());
            //mentionsButton.setText(resources.getString("NextMentions"));
        }
        try{
            task.fillRetweetsTimeline();
        } catch (Exception ex) {
            LOG.error("Unable to display retweets", ex);
        }
    }

    /**
     * handler for the searchbutton click
     * get the tweets using the search term entered
     * @param event 
     */
    @FXML
    void onSearchButtonClick(ActionEvent event) {
        LOG.info("search clicked");
        myProfileListView.getItems().clear();
        if(task == null){
            task = new TwitterTimeLineTask(myProfileListView.getItems());
        }
        try{
            task.fillSearchTimeLine(searchTextField.getText());
        } catch (Exception ex) {
            LOG.error("Unable to display mentions", ex);
        }
    }

    /**
     * Handler for the tweetsretweeted button click
     * get the tweets of the user that have been retweeted
     * @param event 
     */
    @FXML
    void onTweetsRetweetedButtonClick(ActionEvent event) {
        LOG.info("tweets retweeted clicked");    
        myProfileListView.getItems().clear();
        if(task == null){
            task = new TwitterTimeLineTask(myProfileListView.getItems());
            //mentionsButton.setText(resources.getString("NextMentions"));
        }
        try{
            task.fillTweetsRetweetedTimeline();
        } catch (Exception ex) {
            LOG.error("Unable to display retweets", ex);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert myProfileBorderPane != null : "fx:id=\"myProfileBorderPane\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert myProfileImageView != null : "fx:id=\"myProfileImageView\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert mentionsButton != null : "fx:id=\"mentionsButton\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert retweetsButton != null : "fx:id=\"retweetsButton\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert tweetsRetweetedButton != null : "fx:id=\"tweetsRetweetedButton\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert getSavedTweetsButton != null : "fx:id=\"getSavedTweetsButton\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert searchTextField != null : "fx:id=\"searchTextField\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert searchButton != null : "fx:id=\"searchButton\" was not injected: check your FXML file 'myprofile.fxml'.";
        assert myProfileListView != null : "fx:id=\"myProfileListView\" was not injected: check your FXML file 'myprofile.fxml'.";

        myProfileBorderPane.setCenter(getHBoxView());
    }
    /**
     * creates the view for a tweet
     * @return 
     */
    private Node getHBoxView() {
        HBox node = new HBox();
        node.setPadding(new Insets(0, 10, 10, 10));
        node.setSpacing(10);
        ObservableList<TwitterInfoInterface> list = FXCollections.observableArrayList();
        
        myProfileListView.setItems(list);
        myProfileListView.setPrefWidth(900);
        myProfileListView.setCellFactory(p -> new TwitterInfoCell());
        
        node.getChildren().addAll(myProfileListView);

        // Event handler when a cell is selected
       /** listView.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends TwitterInfo> ov, TwitterInfo t, TwitterInfo t1) -> {
                    if (t != null) {
                        LOG.debug("Previous handle: " + t.getHandle());
                    }
                    if (t1 != null) {
                        LOG.debug("New handle: " + t1.getHandle());
                    }
                }); **/
        return node;
    }
    /**
     * sets the image to the imageview of the user
     * @throws TwitterException 
     */
    public void setMyImage() throws TwitterException{
        this.user = twitter.showUser(twitter.getId());
        Image image = new Image(this.user.get400x400ProfileImageURL());
        this.myProfileImageView.imageProperty().set(image);
    }
}
