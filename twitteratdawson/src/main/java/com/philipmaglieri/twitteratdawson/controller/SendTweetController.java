/**
 * controller for the send tweet fxml
 */
package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.business.TwitterEngine;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class SendTweetController {

    private final static Logger LOG = LoggerFactory.getLogger(SendTweetController.class);
    private final TwitterEngine twitterEngine = new TwitterEngine();
    

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="tweetBorderPane"
    private BorderPane tweetBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="sendTweetButton"
    private Button sendTweetButton; // Value injected by FXMLLoader

    @FXML // fx:id="tweetTextArea"
    private TextArea tweetTextArea; // Value injected by FXMLLoader

    /**
     * handler for sending the tweet
     * @param event 
     */
    @FXML
    void onSendTweetButtonClick(ActionEvent event) {
        LOG.info("Send button clicked");
        try {
            twitterEngine.createTweet(tweetTextArea.getText());
            tweetTextArea.setText("");
        } catch (TwitterException ex) {
            LOG.error("unable to send tweet", ex);
        }
    }

    /**
     * add the listener to the button after initializing
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert tweetBorderPane != null : "fx:id=\"tweetborderPane\" was not injected: check your FXML file 'sendtweet.fxml'.";
        assert sendTweetButton != null : "fx:id=\"sendTweetButton\" was not injected: check your FXML file 'sendtweet.fxml'.";
        assert tweetTextArea != null : "fx:id=\"tweetTextArea\" was not injected: check your FXML file 'sendtweet.fxml'.";

        setMaxCharactersTextArea(tweetTextArea);
    }

    /**
     * makes sure that the tweet character limit of 280 is in check
     */
    public void setMaxCharactersTextArea(TextArea tweetTextArea) {
        final int MAX_CHARACTERS = 280;
        tweetTextArea.setWrapText(true);
        tweetTextArea.textProperty().addListener(
                (textAreaObservable, oldValue, newValue) -> {
                    if (newValue.length() > MAX_CHARACTERS) {
                        tweetTextArea.setText(oldValue);
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Error");
                        alert.setHeaderText("Twitter error!");
                        alert.setContentText("Tweet cannot exceed 280 characters.");
                        alert.showAndWait();
                    }
                }
        );

    }
}
