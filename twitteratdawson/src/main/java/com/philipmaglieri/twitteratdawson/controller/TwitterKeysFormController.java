package com.philipmaglieri.twitteratdawson.controller;

/**
 * This controller class handles the form that is needed to be filled in case 
 * the properties file is missing. It will then create the file and insert the 
 * required information from the file.
 * 
 * @author philip
 * inspired by https://gitlab.com/omniprof/javafx_properties_demo/tree/master/src/main/java/com/kenfogel/javafx_properties_demo
 */

import com.philipmaglieri.twitteratdawson.beans.Twitter4jPropertiesBean;
import com.philipmaglieri.twitteratdawson.business.PropertiesManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TwitterKeysFormController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="consumerKey"
    private TextField consumerKey; // Value injected by FXMLLoader

    @FXML // fx:id="consumerSecret"
    private TextField consumerSecret; // Value injected by FXMLLoader

    @FXML // fx:id="accessToken"
    private TextField accessToken; // Value injected by FXMLLoader

    @FXML // fx:id="accessTokenSecret"
    private TextField accessTokenSecret; // Value injected by FXMLLoader
    
    private final Twitter4jPropertiesBean bean;
    private final static Logger LOG = LoggerFactory.getLogger(TwitterKeysFormController.class);
    Scene scene;
    Stage stage;
    MainUIController mainUIController;
    
    /**
     *  constructor sets the bean for use
     */
    
    public TwitterKeysFormController(){
        super();
        bean = new Twitter4jPropertiesBean();
    }
    
    /**
     * sets the scene stage and controller for use by this class 
     * @param scene
     * @param stage
     * @param mainUIController 
     */
    public void changeSceneStage(Scene scene, Stage stage, MainUIController mainUIController){
        this.scene = scene;
        this.stage = stage;
        this.mainUIController = mainUIController;
    }
    
    /**
     * Event handler for Cancel button Exit the program
     */            
    @FXML
    void onCancelClick(ActionEvent event) {
        Platform.exit();
    }
    
    /**
     * Event handler for Clear button
     *
     * @param event
     */
    @FXML
    void onClearClick(ActionEvent event) {
        bean.setAccessToken("");
        bean.setAccessTokenSecret("");
        bean.setConsumerKey("");
        bean.setConsumerSecret("");
    }
    
    /**
     * Event handler for Save button
     *
     * @param event
     */
    @FXML
    void onSaveClick(ActionEvent event) {
        PropertiesManager manager= new PropertiesManager();
        try{
            manager.writeTwitterProperties("", "twitter4j", bean);
            //change to main ui after writing
            stage.setScene(scene);
        }
        catch(IOException exception){
            LOG.error("onSaveClick error",exception); 
        }
    }

    /**
     * binds the bean properties to the tokens in the form 
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert consumerKey != null : "fx:id=\"consumerKey\" was not injected: check your FXML file 'twitterkeysform.fxml'.";
        assert consumerSecret != null : "fx:id=\"consumerSecret\" was not injected: check your FXML file 'twitterkeysform.fxml'.";
        assert accessToken != null : "fx:id=\"accessToken\" was not injected: check your FXML file 'twitterkeysform.fxml'.";
        assert accessTokenSecret != null : "fx:id=\"accessTokenSecret\" was not injected: check your FXML file 'twitterkeysform.fxml'.";
        
        Bindings.bindBidirectional(accessToken.textProperty(),bean.accessTokenProperty());
        Bindings.bindBidirectional(accessTokenSecret.textProperty(),bean.accessTokenSecretProperty());
        Bindings.bindBidirectional(consumerKey.textProperty(),bean.consumerKeyProperty());
        Bindings.bindBidirectional(consumerSecret.textProperty(),bean.consumerSecretProperty());
    }
}

