/**
 * Controller to receive direct messages
 */
package com.philipmaglieri.twitteratdawson.controller;

import com.philipmaglieri.twitteratdawson.business.TwitterEngine;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.TwitterException;

public class ReceiveDirectMessageController {

    private final static Logger LOG = LoggerFactory.getLogger(SendTweetController.class);
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private List<DirectMessage> messages;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="receivedirectMessagesPane"
    private BorderPane receivedirectMessagesPane; // Value injected by FXMLLoader

    @FXML // fx:id="receiveHandleText"
    private TextField receiveHandleText; // Value injected by FXMLLoader

    @FXML // fx:id="receiveButton"
    private Button receiveButton; // Value injected by FXMLLoader

    @FXML // fx:id="receiveTextArea"
    private TextArea receiveTextArea; // Value injected by FXMLLoader

    /**
     * Handler for the receive button gets the most recent direct messages
     * @param event 
     */
    @FXML
    void onReceiveClick(ActionEvent event) {
        LOG.info("receive DM");
        try {
            messages = twitterEngine.getDirectMessage();
           /**for(var message : messages){
               LOG.info(message.toString());
           }**/
        } catch (TwitterException ex) {
            LOG.error("unable to send DM", ex);
        }
        
        String allMessages="";
        for (DirectMessage message : messages){
            
            allMessages += message.getCreatedAt()+ " " + message.getText() +  "\n";
            
        }
        receiveTextArea.setText(allMessages);
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert receivedirectMessagesPane != null : "fx:id=\"receivedirectMessagesPane\" was not injected: check your FXML file 'receivedirectmessage.fxml'.";
        assert receiveHandleText != null : "fx:id=\"receiveHandleText\" was not injected: check your FXML file 'receivedirectmessage.fxml'.";
        assert receiveButton != null : "fx:id=\"receiveButton\" was not injected: check your FXML file 'receivedirectmessage.fxml'.";
        assert receiveTextArea != null : "fx:id=\"receiveTextArea\" was not injected: check your FXML file 'receivedirectmessage.fxml'.";

    }
}
