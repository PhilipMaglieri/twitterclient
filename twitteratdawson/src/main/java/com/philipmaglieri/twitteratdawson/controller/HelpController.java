/**
 * Sample Skeleton for 'help.fxml' Controller Class
 */

package com.philipmaglieri.twitteratdawson.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class HelpController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="helpBorderPane"
    private BorderPane helpBorderPane; // Value injected by FXMLLoader
    
    @FXML // fx:id="helpLabel"
    private Label helpLabel; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert helpBorderPane != null : "fx:id=\"helpBorderPane\" was not injected: check your FXML file 'help.fxml'.";
        assert helpLabel != null : "fx:id=\"helpLabel\" was not injected: check your FXML file 'help.fxml'.";
    }
}
