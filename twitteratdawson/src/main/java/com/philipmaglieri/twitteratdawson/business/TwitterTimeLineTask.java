/**
 * Task of retrieving user's timeline
 * 
 * based on
 * https://gitlab.com/omniprof/tweetdemo02/blob/master/src/main/java/com/kenfogel/tweetdemo02/business/TwitterTimelineTask.java
 * 
 * @author tomo
 */

package com.philipmaglieri.twitteratdawson.business;
import com.philipmaglieri.twitteratdawson.persistence.*;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * Task of retrieving user's timeline
 *
 * @author tomo
 */
public class TwitterTimeLineTask {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterTimeLineTask.class);
    private final ObservableList<TwitterInfoInterface> list;
    private final TwitterEngine twitterEngine;
    private int timelinePage;
    private int DatabasePage;
    private int mentionsPage;
    private int searchPage;
    private int retweetsPage;
    private int tweetsRetweetedPage;
    
    /**
     * Non-default constructor initializes instance variables.
     *
     * @param list
     */
    public TwitterTimeLineTask(ObservableList<TwitterInfoInterface> list) {
        twitterEngine = new TwitterEngine();
        this.list = list;
        timelinePage = 1;
        DatabasePage = 1;
        mentionsPage = 1;
        searchPage = 1;
        retweetsPage = 1;
        tweetsRetweetedPage = 1;
    }

    /**
     * Add new Status objects to the ObservableList. Additions occur at the end
     * of the list.
     *
     * @throws Exception
     */
    public void fillTimeLine() throws Exception {
        List<Status> homeline = twitterEngine.getTimeLine(timelinePage);
        homeline.forEach((status) -> {
            list.add(list.size(), new TwitterInfo(status));
        });
        timelinePage += 1;
    }
    
    /**
     * uses DAO object to access the local database then add it to the list
     * instead of getting tweets from twitter api
     * @throws SQLException 
     */
    public void fillTimeLineFromDatabase() throws SQLException {
        TweetDAOImpl database = new TweetDAOImpl();
        List<TwitterInfoNoStatus> tweets  =  database.findAll();
        tweets.forEach((TwitterInfoNoStatus) -> {
            list.add(list.size(), new TwitterInfoNoStatus(TwitterInfoNoStatus.getName(),
            TwitterInfoNoStatus.getText(),
            TwitterInfoNoStatus.getImageURL(),
            TwitterInfoNoStatus.getHandle(),
            TwitterInfoNoStatus.getTweetId()));
        });
        DatabasePage += 1;
    }
    
    /**
     * uses a similar idea to the original method fillTimeLine but instead
     * uses tweets where the user is mentioned
     * @throws Exception 
     */
    public void fillMentionsTimeLine() throws Exception {
        List<Status> mentionline = twitterEngine.getMentions(mentionsPage);
        mentionline.forEach((status) -> {
            list.add(list.size(), new TwitterInfo(status));
        });
        mentionsPage += 1;
    }
    
    /**
     * uses a similar idea to the original method fillTimeLine but instead
     * uses tweets that include the searchTerm
     * @param searchTerm what the user wants to search
     * @throws Exception 
     */
    public void fillSearchTimeLine(String searchTerm) throws Exception {
        List<Status> searchline = twitterEngine.searchTweets(searchPage,searchTerm);
        searchline.forEach((status) -> {
            list.add(list.size(), new TwitterInfo(status));
        });
        searchPage += 1;
    }
    
    public void fillRetweetsTimeline() throws Exception {
        List<Status> tweetline = twitterEngine.getRetweets(retweetsPage);
        tweetline.forEach((status) -> {
            list.add(list.size(), new TwitterInfo(status));
        });
        retweetsPage += 1;
    }
    
    public void fillTweetsRetweetedTimeline()throws Exception{
        List<Status> tweetline = twitterEngine.getMyTweetsReweeted(tweetsRetweetedPage);
        tweetline.forEach((status) -> {
            list.add(list.size(), new TwitterInfo(status));
        });
        tweetsRetweetedPage += 1;
    }
}
