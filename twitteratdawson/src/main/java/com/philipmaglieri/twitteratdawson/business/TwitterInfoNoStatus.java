/*
 * Mimics the twitterinfo class 
 * by getting the required information to be displayed 
 * from the database
 */
package com.philipmaglieri.twitteratdawson.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.TwitterException;

/**
 *
 * @author 1334608
 */
public class TwitterInfoNoStatus implements TwitterInfoInterface {
    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfo.class);
    
    private String name;
    private String text;
    private String imageURL;
    private String handle;
    private Long tweetId;
    
    
    public TwitterInfoNoStatus(String name, String text, String imageURL, String handle, Long tweetId) {
        this.name = name;
        this.text = text;
        this.imageURL = imageURL;
        this.handle = handle;
        this.tweetId = tweetId;
    }
    public TwitterInfoNoStatus(){
        
    }

    @Override
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getText(){
        return this.text;
    }
    
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getImageURL(){
        return this.imageURL;
    }
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    
    @Override
    public String getHandle() {
      return this.handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    
    @Override
    public long getTweetId() {
        return this.tweetId;
    }
    
    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }
    
    public void like() throws TwitterException{
       // twitter.createFavorite(status.getId());
    }
    
    /**
     * retweets with no comment
     * @throws TwitterException 
     */
    public void retweetNoComment()throws TwitterException{
        //twitter.retweetStatus(status.getId());
    }
}
