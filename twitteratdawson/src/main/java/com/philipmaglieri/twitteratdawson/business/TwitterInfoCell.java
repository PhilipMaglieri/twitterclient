/**
 * ListCell for TwitterInfo This class represents the contents of an HBox that
 * contains tweet info
 *
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 *
 * based on
 * https://gitlab.com/omniprof/tweetdemo02/blob/master/src/main/java/com/kenfogel/tweetdemo02/business/TwitterInfoCell.java
 *
 * @author tomo
 * @author Ken Fogel
 * @author Philip Maglieri
 */
package com.philipmaglieri.twitteratdawson.business;

import com.philipmaglieri.twitteratdawson.controller.MainUIController;
import com.philipmaglieri.twitteratdawson.controller.ProfileController;
import com.philipmaglieri.twitteratdawson.controller.SendTweetController;
import com.philipmaglieri.twitteratdawson.persistence.*;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class TwitterInfoCell extends ListCell<TwitterInfoInterface> {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfoCell.class);
    private final TwitterEngine engine = new TwitterEngine();
    private ProfileController profileController;
    private SendTweetController sendTweetController = new SendTweetController();
    private Stage stage;
    private ResourceBundle resources;

    /**
     * This method is called when ever cells need to be updated
     *
     * @param item
     * @param empty
     */
    @Override
    protected void updateItem(TwitterInfoInterface item, boolean empty) {
        super.updateItem(item, empty);

        //LOG.debug("updateItem");
        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(getTwitterInfoCell(item));
        }
    }

    /**
     * This method determines what the cell will look like. Here is where you
     * can add buttons or any additional information
     *
     * @param info
     * @return The node to be placed into the ListView
     */
    private Node getTwitterInfoCell(TwitterInfoInterface info) {
        //LOG.debug("getTwitterInfoCell");
        loadController(info);
        profileController.setUser(info);
        HBox node = new HBox();
        node.setSpacing(5);

        //Image image = new Image(info.getImageURL(), 48, 48, true, false);
        //ImageView imageView = new ImageView(image);
        Button button = new Button();
        button.setStyle("-fx-background-image: url(" + info.getImageURL() + ");");
        button.setPrefSize(48, 48);
        button.setTooltip(new Tooltip(resources.getString("ProfileImgToolTip")));
        button.setOnAction(event -> {
            stage.showAndWait();
        });

        Button saveButton = new Button();
        //saveButton.getStyleClass().add("buttonPic");
        saveButton.setId("saveButton");
        saveButton.setPrefSize(48, 48);
        saveButton.setTooltip(new Tooltip(resources.getString("SaveToDBToolTip")));
        saveButton.setOnAction(event -> {
            LOG.info("save clicked" + info.getHandle() + info.getText());
            TweetDAOImpl db = new TweetDAOImpl();
            try {
                db.create((TwitterInfo) info);
                LOG.info("tweet saved");
            } catch (SQLException ex) {
                LOG.error("failed to save", ex);
            }

        });

        Text name = new Text(info.getName());
        name.setFill(Color.WHITE);
        name.setFont(Font.font("verdana", FontWeight.EXTRA_BOLD, USE_PREF_SIZE));
        
        name.setWrappingWidth(600);

        Text text = new Text(info.getText());
        text.setFill(Color.WHITE);
        text.setFont(Font.font("verdana", FontWeight.EXTRA_BOLD, USE_PREF_SIZE));
        text.setWrappingWidth(600);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(name, text, makeButtons(info));
        node.getChildren().addAll(button, vbox, saveButton);

        return node;
    }

    /**
     * creates buttons to be shown on the timeline uncder the tweets
     *
     * @return node hbox with buttons
     */
    private Node makeButtons(TwitterInfoInterface info) {
        HBox node = new HBox();
        node.setPadding(new Insets(0, 10, 10, 10));
        node.setSpacing(10);

        TextArea comment = new TextArea();
        comment.setMaxSize(USE_PREF_SIZE, 50.0);
        comment.setWrapText(true);
        comment.setText("");
        sendTweetController.setMaxCharactersTextArea(comment);

        Button reply = new Button("reply");
        reply.getStyleClass().add("tweetButtons");
        reply.setId("reply");
        reply.setTooltip(new Tooltip(resources.getString("ReplyToolTip")));
        reply.setOnAction(event -> {
            try {
                engine.reply("" + info.getTweetId(), comment.getText());
                comment.setText("");
            } catch (TwitterException ex) {
                LOG.error("could not reply", ex);
            }
            
        });
        Button retweet = new Button("retweet");
        retweet.getStyleClass().add("tweetButtons");
        retweet.setId("retweet");
        retweet.setTooltip(new Tooltip(resources.getString("RetweetToolTip")));
        retweet.setOnAction(event -> {
            try {
                if (comment.getText().equals("")) {
                    engine.retweetNoComment(info.getTweetId());
                } else {
                    String url = "https://twitter.com/" + info.getHandle() + "/status/" + info.getTweetId();
                    engine.createTweet(comment.getText() + " " + url);
                    comment.setText("");
                }
            } catch (TwitterException ex) {
                LOG.error("could not retweet", ex);
            }
            
        });
        Button like = new Button("like");
        like.getStyleClass().add("tweetButtons");
        like.setId("like");
        like.setTooltip(new Tooltip(resources.getString("LikeToolTip")));
        like.setOnAction(event -> {
            try {
                engine.like(info.getTweetId());
            } catch (TwitterException e) {
                LOG.error("already liked", e);
            }
        });
        node.getChildren().addAll(reply, like, retweet, comment);
        return node;
    }

    private void loadController(TwitterInfoInterface info) {

        try {
            this.stage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainUIController.class.getResource("/fxml/profile.fxml"));
            
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
            this.resources = ResourceBundle.getBundle("MessagesBundle");
            BorderPane profileBorderPane = (BorderPane) loader.load();
            profileController = loader.getController();
            Scene scene = new Scene(profileBorderPane);
            scene.getStylesheets().add("/styles/mainUI.css");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);

        } catch (IOException e) {
            LOG.error(null, e);
            Platform.exit();
        }

    }
}
