package com.philipmaglieri.twitteratdawson.business;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import com.philipmaglieri.twitteratdawson.beans.Twitter4jPropertiesBean;

/**
 * This class will manage the properties required for interfacing with twitter.
 * tokens and keys given from twitter inspired by
 * https://gitlab.com/omniprof/javafx_properties_demo/tree/master/src/main/java/com/kenfogel/javafx_properties_demo
 *
 * @author Philip Maglieri
 */
public class PropertiesManager {

    /**
     * Returns a Twitter4jPropertiesBean object using contents of properties
     * file
     *
     * @param bean info bean
     * @param path Must exist, will not be created
     * @param propertiesFileName properties file name
     * @return The bean with properties
     * @throws IOException
     */
    public final boolean loadTwitterProperties(Twitter4jPropertiesBean bean, final String path, final String propertiesFileName) throws IOException {
        boolean isThere = false;
        Properties properties = new Properties();

        Path propFile = get(path, propertiesFileName + ".properties");

        // File must exist
        if (Files.exists(propFile)) {
            try (InputStream propertiesFileStream = newInputStream(propFile);) {
                properties.load(propertiesFileStream);
            }
            bean.setAccessToken(properties.getProperty("oauth.accessToken"));
            bean.setAccessTokenSecret(properties.getProperty("oauth.accessTokenSecret"));
            bean.setConsumerKey(properties.getProperty("oauth.consumerKey"));
            bean.setConsumerSecret(properties.getProperty("oauth.consumerSecret"));
            isThere = true;
        }
        return isThere;
    }

    /**
     * Creates a file for twitter tokens as properties
     *
     * @param path Must exist, will not be created
     * @param propertiesFileName Name of the file
     * @param twitterProperties The bean to store in properties file
     * @throws IOException
     */
    public final void writeTwitterProperties(final String path, final String propertiesFileName, final Twitter4jPropertiesBean twitterProperties) throws IOException {

        Properties properties = new Properties();

        properties.setProperty("oauth.accessToken", twitterProperties.getAccessToken());
        properties.setProperty("oauth.accessTokenSecret", twitterProperties.getAccessTokenSecret());
        properties.setProperty("oauth.consumerKey", twitterProperties.getConsumerKey());
        properties.setProperty("oauth.consumerSecret", twitterProperties.getConsumerSecret());

        Path propertiesFile = get(path, propertiesFileName + ".properties");

        try (OutputStream propertiesStream = newOutputStream(propertiesFile)) {
            properties.store(propertiesStream, "Twitter Access Properties");
        }
    }
}
