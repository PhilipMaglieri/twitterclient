package com.philipmaglieri.twitteratdawson.business;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.DirectMessage;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Based on code from https://www.baeldung.com/twitter4j inspired by
 * https://gitlab.com/omniprof/tweetdemo01
 * https://gitlab.com/omniprof/tweetdemo02/blob/master/src/main/java/com/kenfogel/tweetdemo02/business/TwitterEngine.java
 * @author Philip
 */
public class TwitterEngine {


    private final static Logger LOG = LoggerFactory.getLogger(TwitterEngine.class);

    public Twitter getTwitterinstance() {
        /**
         * if not using properties file, we can set access token by following
         * way
         */
//	ConfigurationBuilder cb = new ConfigurationBuilder();
//	cb.setDebugEnabled(true)
//	  .setOAuthConsumerKey("//TODO")
//	  .setOAuthConsumerSecret("//TODO")
//	  .setOAuthAccessToken("//TODO")
//	  .setOAuthAccessTokenSecret("//TODO");
//	TwitterFactory tf = new TwitterFactory(cb.build());
//	Twitter twitter = tf.getSingleton();

        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;

    }

    /**
     * Send a tweet
     *
     * @param tweet string of what to post
     * @return the text sent
     * @throws TwitterException
     */
    public String createTweet(String tweet) throws TwitterException {
        LOG.debug("createTweet: " + tweet);
        Twitter twitter = getTwitterinstance();
        Status status = twitter.updateStatus(tweet);
        return status.getText();
    }
    
    /**
     * Send a reply to a tweet
     * 
     * @param tweetId tweet id
     * @param message reply message
     * @return  the id of the reply
     * @throws TwitterException 
     */
    public String reply(String tweetId,String message) throws TwitterException{
        Twitter twitter = getTwitterinstance();
        Status status = twitter.showStatus(Long.parseLong(tweetId));
        Status reply = twitter.updateStatus(new StatusUpdate(" @" + status.getUser().getScreenName() + " "+ message).inReplyToStatusId(status.getId()));
        return Long.toString(reply.getId());
    }

    /**
     * Get the timeline
     *
     * @param page number of pages to skip
     * @return statuses tweets on hometimeline from the user
     * @throws TwitterException
     */
    public List<Status> getTimeLine(int page) throws TwitterException {
        LOG.debug("getTimeLine");
        Twitter twitter = getTwitterinstance();
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        List<Status> statuses = twitter.getHomeTimeline(paging);
        return statuses;
//        return statuses.stream().map(
//                item -> item.getText()).collect(
//                        Collectors.toList());
    }

    /**
     * Send direct message
     *
     * @param recipientName person to dm
     * @param msg message from user
     * @return directmessage sent 
     * @throws TwitterException
     */
    public String sendDirectMessage(String recipientName, String msg) throws TwitterException {
        LOG.debug("sendDirectMessage");
        Twitter twitter = getTwitterinstance();
        DirectMessage message = twitter.sendDirectMessage(recipientName, msg);
        return message.getText();
    }
    /**
     * overloaded using ID to send DM
     * @param recipientId
     * @param msg string to send
     * @return directmessage sent
     * @throws TwitterException 
     */
    public String sendDirectMessage(long recipientId, String msg) throws TwitterException {
        LOG.debug("sendDirectMessage");
        Twitter twitter = getTwitterinstance();
        DirectMessage message = twitter.sendDirectMessage(recipientId, msg);
        return message.getText();
    }
    
    /**
     * gets the most recent direct messages
     * @return list of messages
     * @throws TwitterException
     */
    public List<DirectMessage> getDirectMessage() throws TwitterException{
        //String cursor = null;
        Twitter twitter = getTwitterinstance();
        List<DirectMessage> messages = twitter.getDirectMessages(50);
        return messages;
    }

    /**
     * Search for tweets with specific contents
     *
     * @param page number of pages to skip
     * @param searchTerm word to search for
     * @return List of tweets according to the query
     * @throws TwitterException
     */
    public List<Status> searchTweets(int page, String searchTerm) throws TwitterException {
        LOG.debug("searchtweets");
        Twitter twitter = getTwitterinstance();
        Query query = new Query(searchTerm);
        //Query query = new Query("source:twitter4j baeldung");
        QueryResult result = twitter.search(query);
        List<Status> statuses = result.getTweets();
        return statuses;
    }

    /**
     * Utility Method to display status
     *
     * @param timeLine
     */
    public void displayTimeLine(List<Status> timeLine) {
        System.out.println("Length of timeline: " + timeLine.size());
        timeLine.stream().map((s) -> {
            System.out.println("User: " + s.getUser().getName());
            return s;
        }).forEachOrdered((s) -> {
            System.out.println("Text: " + s.getSource());
        });
    }
    
    /**
     * Follows the other twitter account
     * @param name person to follow
     * @throws TwitterException 
     */
    public void follow(String name) throws TwitterException{
        Twitter twitter = getTwitterinstance();
        twitter.createFriendship(name);
    }
    
    /**
     * likes the tweet 
     * @param tweetId id of the tweet
     * @throws TwitterException 
     */
    public void like(Long tweetId) throws TwitterException{
        Twitter twitter = getTwitterinstance();
        Status status = twitter.createFavorite(tweetId);
    }
    
    /**
     * retweets with no comment
     * @param tweetId
     * @throws TwitterException 
     */
    public void retweetNoComment(long tweetId) throws TwitterException{
        Twitter twitter = getTwitterinstance();
        Status status = twitter.showStatus(tweetId);
        twitter.retweetStatus(status.getId());
    }
    
     /**
      *  gets the tweets where the user's handle is mentioned
      * @param page number of pages to skip
      * @return List of mentions found
      * @throws TwitterException 
      */ 
    public List<Status> getMentions(int page) throws TwitterException{
        Twitter twitter = getTwitterinstance();
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        List<Status> mentions = twitter.getMentionsTimeline(paging);
        return mentions;
    }
    
    /**
     * finds retweets using the statusId
     * @param page number of pages to skip
     * @return List of retweets found
     * @throws TwitterException 
     */
    public List<Status> getRetweets(int page) throws TwitterException{
        Twitter twitter = getTwitterinstance();
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        List<Status> statuses = twitter.getHomeTimeline(paging);
        List<Status> myTweets = new ArrayList();
        
        for(Status status : statuses){
            if(status.isRetweetedByMe()){
                myTweets.add(status);
            }
        }

        return myTweets;
    }
    
    /**
     * finds the retweets that the logged in user has retweeted
     * @param page page number of pages to skip
     * @return myTweets List of statuses are retweets that the user has retweeted
     * @throws TwitterException 
     */
    public List<Status> getMyTweetsReweeted(int page)throws TwitterException{
        Twitter twitter = getTwitterinstance();
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        List<Status> retweets = twitter.getRetweetsOfMe(paging);
        return retweets;
    }
}
