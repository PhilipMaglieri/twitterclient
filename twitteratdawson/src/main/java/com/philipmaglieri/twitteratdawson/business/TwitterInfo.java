/**
 * Twitter information for ListCell
 * This class defines which members of the Status object you wish to display.
 * 
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 * 
 * based on 
 * https://gitlab.com/omniprof/tweetdemo02/blob/master/src/main/java/com/kenfogel/tweetdemo02/business/TwitterInfo.java
 * 
 * @author tomo
 */

package com.philipmaglieri.twitteratdawson.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class TwitterInfo implements TwitterInfoInterface {
    
    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfo.class);
    
    private final Status status;
    private final Twitter twitter;
    private final TwitterEngine engine = new TwitterEngine();

    public TwitterInfo(Status status) {
        this.status = status;
        this.twitter = engine.getTwitterinstance();
    }

    @Override
    public String getName() {
        return status.getUser().getName();
    }

    @Override
    public String getText(){
        return status.getText();
    }

    @Override
    public String getImageURL(){
        return status.getUser().getProfileImageURL();
    }
    
    @Override
    public String getHandle() {
      return status.getUser().getScreenName();
    }
    
    @Override
    public long getTweetId(){
        return status.getId();
    }
}
