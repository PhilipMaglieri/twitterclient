/*
 * Interface to allow use of twitterinfo or twitterinfonostatus
 * to be used for displaying any timeline
 * 
 */
package com.philipmaglieri.twitteratdawson.business;

/**
 *
 * @author 1334608
 */
public interface TwitterInfoInterface {
    public String getName();
    public String getText();
    public String getImageURL();
    public String getHandle();
    public long getTweetId();
    
}
