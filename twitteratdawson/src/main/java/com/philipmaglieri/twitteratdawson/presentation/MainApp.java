/*
 * This is the main class for the Twitter at Dawson Client.
 * This project is a desktop application for interfacing with Twitter.
 * inspired by https://gitlab.com/omniprof/javafx_properties_demo/tree/master/src/main/java/com/kenfogel/javafx_properties_demo
 */
package com.philipmaglieri.twitteratdawson.presentation;

import com.philipmaglieri.twitteratdawson.beans.Twitter4jPropertiesBean;
import com.philipmaglieri.twitteratdawson.business.PropertiesManager;
import com.philipmaglieri.twitteratdawson.business.TwitterEngine;
import com.philipmaglieri.twitteratdawson.controller.*;

import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import twitter4j.TwitterException;

/**
 *
 * @author 1334608
 */
public class MainApp extends Application {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private final TwitterEngine twitterEngine = new TwitterEngine();

    private MainUIController mainUIController;
    private SendDirectMessageController SendDirectMessageController;
    private SendTweetController sendTweetController;
    private TimeLineController timelineController;

    private BorderPane timelineBorderPane;
    private BorderPane tweetborderPane;
    private BorderPane directMessageBorderPane;
    private Stage stage;

    private Properties properties = new Properties();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Default constructor
     */
    public MainApp() {
        super();
    }

    /**
     * creates 2 scenes in case the properties file is not found so that a form
     * can be filled out to create the file
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        Scene mainScene = makeMainUI();
        Scene formScene = makeForm(mainScene);

        mainScene.getStylesheets().add("/styles/mainUI.css");

        if (!checkPropertiesFile()) {
            LOG.debug("THIS,STAGE,SETSCENE(FORMSCENE)");
            this.stage.setScene(formScene);
        } else {
            LOG.debug("THIS,STAGE,SETSCENE(MAINSCENE)");
            this.stage.setScene(mainScene);
        }
        this.stage.getIcons().add(new Image("/icons/Twitter_Social_Icon_Rounded_Square_Color.png"));
        this.stage.setTitle("Twitter At Dawson");
        LOG.debug("JUST BEFORE SHOW()");
        this.stage.show();
    }

    /**
     * Create a stage with the scene showing the primary program window
     *
     * @return mainScene the scene created
     * @throws Exception due to extending application
     */
    private Scene makeMainUI() throws Exception {
        LOG.debug("MAKEMAINUI() CALLED");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/mainUI.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
        Parent root = (BorderPane) loader.load();
        mainUIController = loader.getController();
        Scene mainScene = new Scene(root);
        mainScene.getStylesheets().add("/styles/mainUI.css");
        return mainScene;
    }

    /**
     * creates a scene for if the form with properties is missing
     *
     * @return formScene form to fill
     */
    private Scene makeForm(Scene scene) throws Exception {
        LOG.debug("MAKEFORM() CALLED");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/TwitterKeysForm.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
        Parent root = (GridPane) loader.load();
        TwitterKeysFormController controller = loader.getController();
        controller.changeSceneStage(scene, stage, mainUIController);
        Scene formScene = new Scene(root);
        return formScene;
    }

    /**
     * Checks for the property file and if it exists loads the properties into a
     * bean.
     *
     * @return isThere is the file there?
     */
    private boolean checkPropertiesFile() {
        LOG.debug("CHECK PROPERTIES FILES () CALLED");
        boolean isThere = false;
        Twitter4jPropertiesBean bean = new Twitter4jPropertiesBean();
        PropertiesManager manager = new PropertiesManager();

        try {
            if (manager.loadTwitterProperties(bean, "", "twitter4j")) {
                isThere = true;
            }
        } catch (IOException exception) {
            LOG.error("property file error", exception);
        }
        return isThere;
    }

}
