/*
 * Class to contain the authentication information for being able to connect
 * with twitter using twitter4j. This should be stored in a properties file.
 * inspired by https://gitlab.com/omniprof/properties_demo/tree/master/src/main/java/com/kenfogel/properties_demo
 */
package com.philipmaglieri.twitteratdawson.beans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * 
 * @author 1334608
 */
public class Twitter4jPropertiesBean {
    private StringProperty consumerKey;
    private StringProperty consumerSecret;
    private StringProperty accessToken ;
    private StringProperty accessTokenSecret;

    /**
     * Default Constructor
     */
    public Twitter4jPropertiesBean() {
        this("", "", "","");
    }

    /**
     * sets the tokens to the bean
     * @param consumerKey
     * @param consumerSecret
     * @param accessToken
     * @param accessTokenSecret
     */
    public Twitter4jPropertiesBean(final String consumerKey, final String consumerSecret, final String accessToken, final String accessTokenSecret) {
        super();
        this.consumerKey = new SimpleStringProperty(consumerKey);
        this.consumerSecret = new SimpleStringProperty(consumerSecret);
        this.accessToken = new SimpleStringProperty(accessToken);
        this.accessTokenSecret = new SimpleStringProperty(accessTokenSecret);
    }
    
    /**
     * @return the ConsumerKey
     */
    public final String getConsumerKey() {
        return consumerKey.get();
    }
    /**
     * @param givenString the consumerKey to set
     */
    public final void setConsumerKey(String givenString) {
        consumerKey.set(givenString);
    }
    
    /**
     * @return the consumerSecret
     */
    public final String getConsumerSecret() {
        return consumerSecret.get();
    }
    
    /**
     * @param givenString the consumerSecret to set
     */
    public final void setConsumerSecret(String givenString) {
        consumerSecret.set(givenString);
    } 
    
    /**
     * @return the accessToken
     */
    public final String getAccessToken() {
        return accessToken.get();
    }
    
    /**
     * @param givenString the accessToken to set
     */
    public final void setAccessToken(String givenString) {
        accessToken.set(givenString);
    }
    
    /**
     * @return the accessTokenSecret
     */
    public final String getAccessTokenSecret() {
        return accessTokenSecret.get();
    }
    
    /**
     * @param givenString the accessTokenSecret to set
     */
    public final void setAccessTokenSecret(String givenString) {
        accessTokenSecret.set(givenString);
    }
    
    public StringProperty consumerKeyProperty() {
        return consumerKey;
    }

    public StringProperty consumerSecretProperty() {
        return consumerSecret;
    }

    public StringProperty accessTokenProperty() {
        return accessToken;
    }
    
    public StringProperty accessTokenSecretProperty() {
        return accessTokenSecret;
    }
}
