/*
 * Interface for the DAO pattern 
 * requirement for project are inserting a tweet
 * and getting all tweets
 */
package com.philipmaglieri.twitteratdawson.persistence;

import com.philipmaglieri.twitteratdawson.business.TwitterInfo;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoInterface;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoNoStatus;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author 1334608
 */
public interface TweetDAO {
    public int create(TwitterInfoInterface twitterInfo) throws SQLException;
    
    public List<TwitterInfoNoStatus> findAll() throws SQLException;
}
