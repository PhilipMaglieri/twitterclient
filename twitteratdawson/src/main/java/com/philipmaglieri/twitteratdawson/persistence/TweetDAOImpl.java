/*
 * Implementaion of the DAO interface
 * 
 */
package com.philipmaglieri.twitteratdawson.persistence;

import com.philipmaglieri.twitteratdawson.business.TwitterInfo;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoInterface;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoNoStatus;
import com.philipmaglieri.twitteratdawson.controller.SendTweetController;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1334608
 */
public class TweetDAOImpl implements TweetDAO {
    
    private final static Logger LOG = LoggerFactory.getLogger(SendTweetController.class);
    //private final static String URL = "jdbc:mysql://localhost:3306/Tweets?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final static String USER = "phil";
    private final static String PASSWORD = "mytwitterdb";
    private final static String URL = "jdbc:mysql://localhost:3306/Tweets?zeroDateTimeBehavior=convertToNull";

    /**
     * inserts the given info into the database
     * @param twitterInfo
     * @return int number of rows affected
     * @throws SQLException 
     */
    @Override
    public int create(TwitterInfoInterface twitterInfo) throws SQLException{
        
        int result;
        String createQuery = "INSERT INTO Tweet (Name,Text,ImageURL,Handle,TweetId) VALUES (?,?,?,?,?)";

        
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

            PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForTweet(ps, twitterInfo);
            result = ps.executeUpdate();

            // Retrieve generated primary key value and assign to bean
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
            }
        }
        LOG.info("# of records created : " + result);
        return result;

    }
        private void fillPreparedStatementForTweet(PreparedStatement ps, TwitterInfoInterface twitterInfo) throws SQLException {
        ps.setString(1, twitterInfo.getName());
        ps.setString(2, twitterInfo.getText());
        ps.setString(3, twitterInfo.getImageURL());
        ps.setString(4, twitterInfo.getHandle());
        ps.setLong(5, twitterInfo.getTweetId());
    }

    /**
     * gets all the saved tweets 
     * @return List of twitterinfo mimic with no status
     * @throws SQLException 
     */
    @Override
    public List<TwitterInfoNoStatus> findAll() throws SQLException{
        List<TwitterInfoNoStatus> rows = new ArrayList<>();

        String query = "SELECT Name,Text,ImageURL,Handle,TweetId FROM Tweet";
        
         try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement pStatement = connection.prepareStatement(query);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createTweetData(resultSet));
            }
        }
        LOG.info("# of records found : " + rows.size());
        return rows;
    }
    
    private TwitterInfoNoStatus createTweetData(ResultSet resultSet)throws SQLException{
        TwitterInfoNoStatus twitterInfoNoStatus = new TwitterInfoNoStatus();
        
        twitterInfoNoStatus.setName(resultSet.getString("Name"));
        twitterInfoNoStatus.setText(resultSet.getString("Text"));
        twitterInfoNoStatus.setImageURL(resultSet.getString("ImageURL"));
        twitterInfoNoStatus.setHandle(resultSet.getString("Handle"));
        twitterInfoNoStatus.setTweetId(resultSet.getLong("TweetId"));
        
        return twitterInfoNoStatus;
    }
}
