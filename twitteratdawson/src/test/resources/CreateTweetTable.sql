DROP TABLE IF EXISTS Tweets.Tweet;

CREATE TABLE Tweets.Tweet (
  Name varchar(50) NOT NULL default '',
  Text varchar(280) NOT NULL default '',
  ImageURL varchar(200) NOT NULL default '',
  Handle varchar(15) NOT NULL default '',
  TweetId bigint,
  primary key (TweetId)
); 
