DROP DATABASE IF EXISTS Tweets;
CREATE DATABASE Tweets;

USE Tweets;

DROP USER IF EXISTS phil@localhost;
CREATE USER phil@'localhost' IDENTIFIED BY 'mytwitterdb';
GRANT ALL ON Tweets.* TO phil@'localhost';

FLUSH PRIVILEGES;