/*
 * Test Class for the DAO objects
 * 
 * 
 */
package tests;

import com.philipmaglieri.twitteratdawson.persistence.*;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoNoStatus;
import com.philipmaglieri.twitteratdawson.business.TwitterInfo;
import com.philipmaglieri.twitteratdawson.business.TwitterInfoInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * based on https://gitlab.com/omniprof/jdbc_demo/blob/master/src/test/java/com/kenfogel/jdbcdemo/unittests/DemoTestCase.java
 * @author philip
 */
public class TwitterTests {
    private final static Logger LOG = LoggerFactory.getLogger(TwitterTests.class);
    private final static String USER = "phil";
    private final static String PASSWORD = "mytwitterdb";
    private final static String URL = "jdbc:mysql://localhost:3306/Tweets?zeroDateTimeBehavior=convertToNull";
    
    /**
     * no data request 
     * @throws SQLException 
     */
    @Test (timeout = 1000)
    public void testFindAllNoData() throws SQLException{
        TweetDAO database = new TweetDAOImpl();
        List<TwitterInfoNoStatus> tweets =database.findAll();
        int rows = tweets.size();
        assertEquals("table was not empty",0,rows);
    }
    
    /**
     * standard test to verify that findall() works 
     * @throws SQLException 
     */
    @Test (timeout = 1000)
    public void testFindAllWithData() throws SQLException{
        long num = 655;
        TwitterInfoInterface tweet = new TwitterInfoNoStatus("bob",
                "hello there",
                "imageURL",
                "handle",
                num);
        long num2 = 400;
        TwitterInfoInterface tweet2 = new TwitterInfoNoStatus("john",
                "general kenobi",
                "imageURL",
                "handle",
                num2);
        TweetDAO database = new TweetDAOImpl();
        database.create(tweet);
        database.create(tweet2);
        List<TwitterInfoNoStatus> tweets = database.findAll();
        int rows = tweets.size();
        assertEquals("did not return the correct amount", 2,rows);
    }
    
    /**
     * test to not create a tweet with the same id in the database
     * @throws SQLException 
     */
    @Test (timeout = 1000,expected = SQLException.class)
    public void testCreateSameId() throws SQLException{
        long num = 655;
        TwitterInfoInterface tweet = new TwitterInfoNoStatus("bob",
                "hello there",
                "imageURL",
                "handle",
                num);
        long num2 = 655;
        TwitterInfoInterface tweet2 = new TwitterInfoNoStatus("john",
                "general kenobi",
                "imageURL",
                "handle",
                num2);
        TweetDAO database = new TweetDAOImpl();
        database.create(tweet);
        database.create(tweet2);
        fail("tweet had the same ID and did not throw an error");
    }
    
    /**
     * Standard test to verify that create() works
     * @throws SQLException 
     */
    @Test(timeout = 1000)
    public void testCreate() throws SQLException{
        long num = 655;
        TwitterInfoInterface tweet = new TwitterInfoNoStatus("bob",
                "hello there",
                "imageURL",
                "handle",
                num);
        TweetDAO database = new TweetDAOImpl();
        int numAffected = database.create(tweet);
        List<TwitterInfoNoStatus> tweet2 = database.findAll();
        int rows = tweet2.size();
        assertEquals("A record was not created", numAffected, rows);
    }
      
    
    @AfterClass
    public static void seedAfterTestCompleted() {
        LOG.info("@AfterClass seeding");
        new TwitterTests().seedDatabase();
    }
        
    @Before 
    public void seedDatabase() {
        LOG.info("@Before seeding");

        final String seedDataScript = loadAsString("CreateTweetTable.sql");
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

}
